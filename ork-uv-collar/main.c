/* dr. Ork UV Collar
 * Copyright (C) 2023 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "leds.h"
#include "light_map.h"

#include <math.h>
#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <inttypes.h>
#include <util/atomic.h>
#include <util/delay.h>

/** Pin-out:
 *
 * PB0: IN  Button on main board
 * PB3: OUT PWM to LED gate driver
 * PB4: IN  Button on external board
 */

volatile bool btn_extern_pressed = false;
volatile bool btn_onboard_pressed = false;

void setup();

void loop()
{
  const bool btn_pressed = btn_extern_pressed || btn_onboard_pressed;
  static int16_t brightness_int;

  const float woefje_max = 0.1f;
  static float woefje = 0.0f;
  static float woefje_delta = 0.001f;
  static float woefje_dir = 1.0f;

  if (btn_pressed) {
    if (brightness_int < 500)
      brightness_int += 3;
  }
  else {
    if (brightness_int > 0)
      brightness_int--;
  }

  woefje += woefje_dir * woefje_delta;
  if (woefje > woefje_max) {
    woefje = woefje_max;
    woefje_dir = -1;
  }
  else if (woefje < -woefje_max) {
    woefje = -woefje_max;
    woefje_dir = 1;
  }

  if (brightness_int > 400) {
    woefje_delta = 0.003f;
  }

  const float brightness_float = brightness_int / 250.0f;
  const float brightness_clamped = fmaxf(0.0f, fminf(brightness_float, 0.9f) + woefje);

  const int pwm_value = light_map_lookup(brightness_clamped);
  led_set_pwm(pwm_value);

  if (brightness_int == 0 && !btn_pressed) {
    woefje_delta = 0.001f;
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_mode();
  }

  _delay_ms(8);
}

int main()
{
  setup();
  sei();

  for (;;)
    loop();
  return 0;
}

ISR(PCINT0_vect)
{
  const uint8_t portb = PINB;
  btn_extern_pressed = (portb & (1 << PB4)) == 0;
  btn_onboard_pressed = (portb & (1 << PB0)) == 0;
}

void setup()
{
  // Enable watchdog.
  MCUSR = 0x00;
  WDTCR |= (1 << WDCE) | (1 << WDE);
  WDTCR = 0x00;

  // Set clock speed.
  CLKPR = 1 << CLKPCE;
  CLKPR = 0b0011 << CLKPS0;

  DDRB = 1 << DDB3;    // Output on PB3.
  PORTB = 0b00110111;  // Pull-up resistors on all inputs for stable pins.

  PRR = 0              //
        | 1 << PRTIM0  // Disable Timer 0
        | 1 << PRUSI   // Disable USI
        | 1 << PRADC   // Disable ADC
      ;

  // Set pin change interrupt on PB0 and PB4.
  GIMSK = 0            //
          | 1 << PCIE  //
      ;
  PCMSK = 0              //
          | 1 << PCINT0  // PB0
          | 1 << PCINT4  // PB4
      ;

  leds_setup();
  led_set_clock(1);
}
