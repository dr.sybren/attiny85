/* Keukenlicht-ATtiny85
 * Copyright (C) 2019 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "light_map.h"

#include <stdint.h>

// 8-bit PWM light map
//   total steps         : 64
//   initial single steps: 13
static const uint8_t light_map_8bit[] = {
    0,   1,   2,   3,   4,   5,   6,   7,   8,   9,   10,  11,  12,  13,  14,  15,
    16,  17,  18,  19,  20,  21,  23,  24,  26,  27,  29,  31,  33,  35,  37,  39,
    42,  44,  47,  50,  53,  56,  60,  63,  67,  71,  76,  80,  85,  90,  95,  101,
    107, 114, 120, 128, 135, 143, 152, 161, 170, 180, 191, 203, 215, 227, 241, 255};
// static const uint8_t light_map_8bit_size = 64;
static const uint8_t light_map_8bit_max_index = 63;

uint8_t light_map_lookup(const float value /* range [0, 1] */)
{
  if (value <= 0.0f) {
    return light_map_8bit[0];
  }

  const float index = value * light_map_8bit_max_index;
  const uint8_t lower_index = index;
  if (lower_index >= light_map_8bit_max_index) {
    return light_map_8bit[light_map_8bit_max_index];
  }

  const uint8_t lower_light_map_value = light_map_8bit[lower_index];
  const uint8_t upper_light_map_value = light_map_8bit[lower_index + 1];

  const float alpha = index - lower_index;
  const float interpolated = (1.0f - alpha) * lower_light_map_value +
                             alpha * upper_light_map_value;
  return interpolated + 0.5f;
}
