/* dr. Ork UV Collar
 * Copyright (C) 2023 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "leds.h"

#include <avr/io.h>

void leds_setup()
{
  // Set to values that correspond to as-off-as-possible once the timer config is in.
  OCR1B = 255;

  // Pin PB3 (~OC1B) gets PWM.
  TCCR1 = 0b0000 << CS10;   // Clock select is delegated to state functions
  GTCCR = 0                 //
          | 1 << PWM1B      // enable PWM1B
          | 0b01 << COM1B0  // Only mode in which ~OC1B is connected.
      ;
  TIMSK = 0;
  TIFR = 0;
}

void led_set_pwm(const uint8_t pwm_value)
{
  OCR1B = 255 - pwm_value;
}

void led_set_clock(const uint8_t clock_select)
{
  TCCR1 = (TCCR1 & ~(0b1111 << CS10)) | (clock_select << CS10);
}
