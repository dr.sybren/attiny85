#include "leds.h"
#include "light_map.h"
#include "state_machine.h"

#include <avr/io.h>
#include <stddef.h>

static void enter_state()
{
  // Set high clock speed to have NO visual strobing.
  leds_set_clock(0b0100);
}

static void exit_state()
{
}

static void tick(const float smoothed_input)
{
  // The 1.25 factor compensates for the max 0.8 smoothed_input we'll get from main.c
  const uint8_t interpolated_pwm = light_map_lookup(smoothed_input * 1.25f);
  set_pwm_led1(interpolated_pwm);
  set_pwm_led2(interpolated_pwm);
}

State state_normal()
{
  State state = {
      .setup = NULL,
      .enter_state = enter_state,
      .exit_state = exit_state,
      .tick = tick,
  };

  return state;
}
