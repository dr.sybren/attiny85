/* Keukenlicht-ATtiny85
 * Copyright (C) 2019 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "state_machine.h"
#include <stddef.h>

volatile State *current_state = NULL;

void state_switch(State *new_state)
{
  if (current_state == new_state)
    return;

  if (current_state && current_state->exit_state) {
    current_state->exit_state();
  }

  current_state = new_state;

  if (current_state && current_state->enter_state) {
    current_state->enter_state();
  }
}
