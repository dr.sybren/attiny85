#include "leds.h"
#include "light_map.h"
#include "state_machine.h"

#include <avr/io.h>
#include <stddef.h>

static void enter_state()
{
  // Set low clock speed to have visual strobing.
  leds_set_clock(0b1010);

  const uint8_t pwm_value = 255 >> 2;
  set_pwm_led1(pwm_value);
  set_pwm_led2(pwm_value);
}

static void exit_state()
{
}

static void tick(const float smoothed_input)
{
}

State state_strobe()
{
  State state = {
      .setup = NULL,
      .enter_state = enter_state,
      .exit_state = exit_state,
      .tick = tick,
  };

  return state;
}
