/* Keukenlicht-ATtiny85
 * Copyright (C) 2019 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "leds.h"
#include "state_blink.h"
#include "state_machine.h"
#include "state_normal.h"
#include "state_strobe.h"

#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <inttypes.h>
#include <util/atomic.h>

/** Pin-out:
 *
 * PB0: OUT LED1 / OC0A / ~OC1A
 * PB3: IN  Pot
 * PB4: OUT LED2 / OC1B
 */

State _state_normal;
State _state_strobe;
State _state_blink;

void setup_adc()
{
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 0b011 << ADPS0  // scale ADC clock
           | 0b000 << REFS0  // VCC as voltage ref
      ;

  // Read from ADC3 == PB3
  ADMUX = 3 << MUX0 | 0 << ADLAR;
}

void setup()
{
  DDRB = 1 << DDB0 | 1 << DDB4;
  PORTB = 0;

  leds_setup();
  setup_adc();

  _state_blink = state_blink();
  if (_state_blink.setup)
    _state_blink.setup();

  _state_strobe = state_strobe();
  if (_state_strobe.setup)
    _state_strobe.setup();

  _state_normal = state_normal();
  if (_state_normal.setup)
    _state_normal.setup();

  current_state = &_state_normal;
  if (current_state->enter_state)
    current_state->enter_state();
}

ISR(ADC_vect)
{
}

void readADC()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  ADCSRA |= 1 << ADSC | 1 << ADIE;
  sleep_mode();
}

inline static float clamp_01(const float value)
{
  if (value <= 0.0f)
    return 0.0f;
  if (value >= 1.0f)
    return 1.0f;
  return value;
}

void loop()
{
  readADC();

  static float smoothed_input = 0.0f;

  const uint16_t adc = ADC;
  const float input = ((float)adc) / 1023.f;
  const float step_size = (input - smoothed_input) / 32.f;
  smoothed_input = clamp_01(smoothed_input + step_size);

  if (smoothed_input < 0.80f) {
    state_switch(&_state_normal);
  }
  else if (smoothed_input < 0.95f) {
    // state_switch(&_state_blink);
    state_switch(&_state_strobe);
  }
  else {
    state_switch(&_state_normal);
  }

  current_state->tick(smoothed_input);
}

int main()
{
  setup();
  sei();

  for (;;)
    loop();
  return 0;
}
