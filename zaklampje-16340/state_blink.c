#include "leds.h"
#include "light_map.h"
#include "state_machine.h"

#include <avr/io.h>
#include <stddef.h>

#define DEFAULT_STEP_SIZE 0.005f
static float brightness = 0;
static float step_size = DEFAULT_STEP_SIZE;

static void enter_state()
{
  // Set high clock speed to have NO visual strobing.
  leds_set_clock(0b0100);

  if (brightness < 0.5f) {
    brightness = 0;
    step_size = DEFAULT_STEP_SIZE;
  }
  else {
    brightness = 1.0f;
    step_size = -DEFAULT_STEP_SIZE;
  }
}

static void exit_state()
{
}

static void tick(const float smoothed_input)
{
  brightness += step_size;
  if (brightness > 1.0f) {
    brightness = 1.0f;
    step_size = -step_size;
  }
  else if (brightness < 0.0f) {
    brightness = 0.0f;
    step_size = -step_size;
  }

  const uint8_t pwm_value_1 = light_map_lookup(brightness);
  const uint8_t pwm_value_2 = light_map_lookup(1.0f - brightness);
  set_pwm_led1(pwm_value_1);
  set_pwm_led2(pwm_value_2);
}

State state_blink()
{
  State state = {
      .setup = NULL,
      .enter_state = enter_state,
      .exit_state = exit_state,
      .tick = tick,
  };

  return state;
}
