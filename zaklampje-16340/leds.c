/* Keukenlicht-ATtiny85
 * Copyright (C) 2019 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "leds.h"

#include <avr/io.h>

void leds_setup()
{
  // // Pins PB0 (OC0A) get PWM.
  // TCCR0A = 0                 //
  //          | 0b10 << COM0A0  // Normal PWM mode on OC0A
  //          | 0b00 << COM0B0  // OCR0B disabled
  //          | 0b11 << WGM00   // fast PWM mode = WGM 011
  //     ;
  // TCCR0B = 0                //
  //          | 0 << WGM02     // fast PWM mode
  //          | 0b010 << CS00  // clk/8
  //     ;

  // // Pin PB4 (OC1B) gets PWM.
  // TCCR1 = 0                 //
  //         | 0 << PWM1A      // disable PWM1A
  //         | 0b00 << COM1A0  // OCR1A pin disconnected
  //         | 0b0100 << CS10  // clk/8
  //     ;
  // GTCCR = 0                 //
  //         | 1 << PWM1B      // enable PWM1B
  //         | 0b10 << COM1B0  // OC1B pin connected normally
  //     ;

  // Set to values that correspond to as-off-as-possible once the timer config is in.
  OCR1A = 255;
  OCR1B = 0;

  // Pins PB0 (~OC1A) and PB4 (OC1B) get PWM.
  TCCR1 = 0                 //
          | 1 << PWM1A      // enable PWM1A
          | 0b01 << COM1A0  // OC1A and ~OC1A pins connected (only way to connect ~OC1A)
          | 0b0000 << CS10  // Clock select is delegated to state functions
      ;
  GTCCR = 0                 //
          | 1 << PWM1B      // enable PWM1B
          | 0b10 << COM1B0  // OC1B pin connected normally
      ;
}

void set_pwm_led1(const uint8_t pwm_value)
{
  // OCR0A = 255 -> off, 254 -> on at dimmest level
  if (pwm_value == 255) {
    OCR1A = 0;
  }
  else {
    OCR1A = 254 - pwm_value;
  }
}

void set_pwm_led2(const uint8_t pwm_value)
{
  // OCR1B = 0 -> off, 1 -> on at dimmest level
  if (pwm_value == 255) {
    OCR1B = 255;
  }
  else {
    OCR1B = 1 + pwm_value;
  }
}

void leds_set_clock(const uint8_t clock_select)
{
  TCCR1 &= ~(0b1111 << CS10);
  TCCR1 |= (clock_select << CS10);
}
