/* ATtiny8555
 * Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "hardware.h"

#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>

#define TRIGGER_FALLING (0b10 << ACIS0)
#define TRIGGER_RISING (0b11 << ACIS0)
#define TRIGGER_TOGGLE (0b00 << ACIS0)
#define TRIGGER_MASK (0b11 << ACIS0)

static void setup(void);
static void loop(void);
static void set_pwm(bool enabled);

static volatile bool current_pwm_state = false;

int main(void)
{
  setup();
  sei();
  for (;;)
    loop();
  return 0;
}

static void setup(void)
{
  /* Disable watchdog timer. */
  MCUSR &= ~(1 << WDRF);
  WDTCR |= (1 << WDCE) | (1 << WDE);
  WDTCR = 0;

  // Configure system clock speed.
  CLKPR = 1 << CLKPCE;
  CLKPR = 0b0000;
  OSCCAL = 180;

  // Configure PWM and nPWM pins as outputs.
  DDRB = (1 << PB3) | (1 << PB4);

  // Start with a low output on both the PWM and nPWM pins. This breaks their
  // relation, but ensures the reference voltage is certainly lower than the
  // triangle input.
  pwm_out(false);
  npwm_out(false);

  // Disable digital input on AIN0 and AIN1.
  DIDR0 |= (1 << ADC0D) | (1 << ADC1D);
  PRR = 0              //
        | 1 << PRTIM1  // Disable timer 1
        | 1 << PRTIM0  // Disable timer 0
        | 1 << PRUSI   // Disable USI
        | 1 << PRADC   // Disable ADC
      ;

  // Disable analog comparator multiplexer to force AIN1 as comparator input.
  ADCSRB = 0;
  ACSR = TRIGGER_TOGGLE;
  ACSR |= 1 << ACIE;  // Enable interrupt after setting trigger direction.

  // Set a high reference voltage, so that the triangle output will rise.
  set_pwm(true);
}

static void loop(void)
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();
}

// Called whenever the comparator toggles.
ISR(ANA_COMP_vect)
{
  const bool higher_than_threshold = ACSR & (1 << ACO);
  set_pwm(higher_than_threshold);
}

static void set_pwm(const bool enabled)
{
  if (enabled) {
    PORTB = (PORTB | (1 << PORTB4)) & ~(1 << PORTB3);
  }
  else {
    PORTB = (PORTB | (1 << PORTB3)) & ~(1 << PORTB4);
  }
}
