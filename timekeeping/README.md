# Timekeeping ATtiny85

This is a small project to play with my timekeeping module. It's intended to run
a timer at a fixed frequency, then count the number of "ticks" as an indication
of the passage of time.

This project allows me to set a clock speed, then adjust the timer parameters to
get the frequency I want, then copy the files into another, bigger project that
actually uses it.
