// SPDX-License-Identifier: GPL-3.0-or-later
#include "hardware.h"
#include "timekeeping.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

int main(void)
{
  // Disable watchdog.
  WDTCR |= 1 << WDCE | 1 << WDE;
  WDTCR = 0 << WDE;

  // Set clock prescaler to go to 2 MHz.
  CLKPR = 1 << CLKPCE;
  CLKPR = 0b0010 << CLKPS0;

  sei();

  timekeeping_setup();

  // Timer 0 is in CTC mode, which means that the overflow interrupt, which
  // causes the pin to toggle, will produce a square wave at half the timer
  // frequency.
  DDRB |= (1 << DDB1) | (1 << DDB0);
  TCCR0A |= (0b01 << COM0A0);  // Toggle OC0A on Compare Match.

#if 0
  // Also output on OC0B:
  OCR0B = OCR0A;
  TCCR0A |= (0b01 << COM0B0);
  for (;;) {
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
#endif

#if 1
  uint16_t last_time = 0;
  bool out_on = false;
  for (;;) {
    while (!timekeeping_ms_periodic(&last_time, 4)) {
      set_sleep_mode(SLEEP_MODE_IDLE);
      sleep_mode();
    }
    out_on = !out_on;
    if (out_on) {
      PORTB |= 1 << PB1;
    }
    else {
      PORTB &= ~(1 << PB1);
    }
  }
#endif
}
