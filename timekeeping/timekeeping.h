// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Use Timer 0 to keep track of time. */

void timekeeping_setup();
void timekeeping_reset();
uint16_t timekeeping_ms_since_reset();

bool timekeeping_ms_passed(uint16_t last_time_ms, uint16_t interval_ms);
bool timekeeping_ms_periodic(uint16_t *last_time_ms, uint16_t interval_ms);
