// SPDX-License-Identifier: GPL-3.0-or-later
#include "timekeeping.h"
#include "hardware.h"

#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

static volatile uint16_t msec_count;

void timekeeping_setup()
{
  // Timer 0 for keeping track of time at 1 kHz.
  // Using T0 because it has the least flexibility in clockdiv but it's good
  // enough here.

  // Values found so far:
  //
  // F_CPU (MHz) | ClkDiv | OCR0A | Freq (Hz, approx)
  // ------------+--------+-------+------------------
  //           2 |   2    | 251   | 1000

  TCCR0A = 0b10 << WGM00;  // CTC mode
  TCCR0B = 0               //
           | 0 << WGM02    // CTC mode
           | 2 << CS00     // clkdiv
      ;
  OCR0A = 251;
  TIMSK |= 1 << OCIE0A;
}

void timekeeping_reset()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCNT0 = 0;
    msec_count = 0;
  }
}

uint16_t timekeeping_ms_since_reset()
{
  int16_t safe_msec_count;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    safe_msec_count = msec_count;
  }
  return safe_msec_count;
}

bool timekeeping_ms_passed(const uint16_t last_time_ms, const uint16_t interval_ms)
{
  const uint16_t now = timekeeping_ms_since_reset();
  const uint16_t expired = now - last_time_ms;
  const bool has_passed = expired > interval_ms;
  return has_passed;
}

bool timekeeping_ms_periodic(uint16_t *last_time_ms, const uint16_t interval_ms)
{
  const uint16_t now = timekeeping_ms_since_reset();
  const uint16_t expired = now - *last_time_ms;
  const bool has_passed = expired > interval_ms;
  if (!has_passed) {
    // Not enough time has passed yet.
    return false;
  }
  *last_time_ms = now;
  return true;
}

ISR(TIMER0_COMPA_vect)
{
  const uint8_t sreg = SREG;

  TCNT0 = 0;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    msec_count++;
  }

  SREG = sreg;
}
