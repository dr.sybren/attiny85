MIN = 500
MAX = 2500

high = 1250
low = 800

low_perc = (low - MIN) / (MAX - MIN)
high_perc = (high - MIN) / (MAX - MIN)
print(f"low : { low:4} = { low_perc*100:.1f}%")
print(f"high: {high:4} = {high_perc*100:.1f}%")


for pos_int in range(0, 101, 20):
    pos = pos_int / 100
    servo = high_perc - (high_perc - low_perc) * pos
    print(f"{pos*100:3.0f} -> {servo*100:.1f}%")
