/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#include <avr/io.h>
#include <stdbool.h>

/* Pinout:
 *
 * PB1: IN/OUT Presence
 * PB2: IN     Switch
 * PB3: OUT Servo lid, servo range = 2000us/75% (closed) to 500us/0% (open).
 * PB4: OUT Servo paw, servo range = 1250us/37.5% (down) to 900us/20% (up)
 */

#define F_CPU 20000000

static inline void hardware_setup()
{
  // Set the expected clockdiv.
  CLKPR = 1 << CLKPCE;
#if F_CPU == 30000000
  // Be sure to enable the PLL as clock source!
  CLKPR = 0b0000;
  OSCCAL = 252;
#elif F_CPU == 20000000
  // Be sure to enable the PLL as clock source!
  CLKPR = 0b0000;
  OSCCAL = 201;
#elif F_CPU == 16000000
  // Be sure to enable the PLL as clock source!
  CLKPR = 0b0000;
  OSCCAL = 147;
#elif F_CPU == 8000000
  CLKPR = 0b0000;
#elif F_CPU == 1000000
  CLKPR = 0b0011;
#else
#  error Unexpected value for F_CPU
#endif
}

static inline bool pin_switch()
{
  return PINB & (1 << PINB2);
}

static inline bool pin_presence()
{
  return PINB & (1 << PINB1);
}

static inline void servo_lid(const bool enable)
{
  if (enable) {
    PORTB |= (1 << PORTB3);
  }
  else {
    PORTB &= ~(1 << PORTB3);
  }
}

static inline void servo_paw(const bool enable)
{
  if (enable) {
    PORTB |= (1 << PORTB4);
  }
  else {
    PORTB &= ~(1 << PORTB4);
  }
}
