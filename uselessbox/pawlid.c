/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "pawlid.h"
#include "servo.h"

void move_paw(const float percentage, const uint8_t speed)
{
  // range = 37.5% (down) to 15% (up)
  servo_set_position(SERVO_PAW, 0.375f - (0.375f - 0.15f) * percentage, speed);
}

void move_lid(const float percentage, const uint8_t speed)
{
  // range = 79% (closed) to 30% (open)
  servo_set_position(SERVO_LID, 0.79f - (0.79f - 0.30f) * percentage, speed);
}
