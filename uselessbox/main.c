/* ATtiny85 Useless Box
 * Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "hardware.h"
#include "pawlid.h"
#include "program.h"
#include "servo.h"
#include "timekeeping.h"

#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/delay.h>

static uint16_t time_of_switch;

static void setup(void);
static void boot(void);
static void loop(void);
static void attack_tick();
static void attack_start();

int main(void)
{
  setup();
  sei();
  boot();
  for (;;) {
    loop();

    // See if servos need some more time to move.
    if (!timekeeping_ms_passed(servo_last_update_time, SERVO_LAG_TIME)) {
      servo_tick();
    }
    else {
      // Nothing to do, sleep around.
      set_sleep_mode(SLEEP_MODE_IDLE);
      sleep_mode();
    }
  }
  return 0;
}

static void setup(void)
{
  MCUSR = 0;  // reset boot flags.

  /* Enable watchdog timer. */
  WDTCR |= (1 << WDCE) | (1 << WDE);
  WDTCR = 0                 //
          | 1 << WDE        // Enable
          | 0b0110 << WDP0  // Timeout of ~1 sec.
      ;

  hardware_setup();

  // Disable all pin change interrupts unless explicitly enabled.
  PCMSK = 0;

  // Configure servo pins as outputs.
  DDRB = (1 << DDB3) | (1 << DDB4);
  PORTB = 0;

  TIMSK = 0;
  GTCCR = 0;

  timekeeping_setup();
  servo_setup();
  program_setup();

  time_of_switch = 0;

  // Configure switch pin on PB2.
  PORTB |= (1 << PORTB2);
  // Wait for the pull-ups to do their work. If this takes too long, it means
  // the switch is ON.
  for (uint8_t i = 0; i < 10 && !pin_switch(); i++) {
    _delay_ms(100);
  }
  DDRB &= ~(1 << DDB2);
  PCMSK |= 1 << PCINT2;
  GIMSK |= 1 << PCIE;
}

static void boot()
{
  // debug_led_out(true);

  // Ensure the servos are in their initial position.
  move_lid(0.3f, 100);
  move_paw(0.0f, 100);
  while (!timekeeping_ms_passed(0, 500)) {
    wdt_reset();
    servo_tick();
  }
  move_lid(0.f, 100);
  move_paw(0.f, 100);

  if (!pin_switch()) {
    // If the switch is still ON, initiate an attack without waiting for pin change interrupt.
    attack_start();
  }
  // debug_led_out(false);
}

static bool is_attacking = false;

static void loop(void)
{
  // debug_led_out(is_attacking);
  wdt_reset();

  if (is_attacking) {
    attack_tick();
    return;
  }

  // Just to see if we missed anything
  if (!pin_switch()) {
    attack_start();
    return;
  }
}

static void attack_tick()
{
  if (!pin_switch()) {  // Switch is ON, attack it!
    program_tick();
    return;
  }

  if (time_of_switch) {
    // Switch time was not yet reset, so this is the first tick after the
    // switch moved to OFF.
    time_of_switch = 0;
    program_start(PROGRAM_TYPE_RELEASE);
  }

  if (program_tick()) {
    // Just run the release program.
    return;
  }

  // Release program is done, attack is over.
  is_attacking = false;
}

ISR(PCINT0_vect)
{
  const uint8_t sreg = SREG;
  attack_start();
  SREG = sreg;
}

static void attack_start()
{
  if (is_attacking) {
    return;
  }

  // Switch just flipped, remember when it happened.
  time_of_switch = timekeeping_ms_since_reset();

  // 0 means "no attack", so ensure it's not that.
  if (time_of_switch == 0)
    time_of_switch = 1;

  program_start(PROGRAM_TYPE_ATTACK);
  is_attacking = true;
}
