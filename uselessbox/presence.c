/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "presence.h"
#include "hardware.h"

#include <avr/io.h>
#include <util/atomic.h>

static volatile uint16_t discharge_count;

void presence_setup()
{
  // Disable pin change interrupt on PB1.
  PCMSK &= ~(1 << PCINT1);

  // PB1 as output.
  DDRB |= (1 << DDB1);
  PORTB &= (1 << PB1);

  discharge_count = 0;
}

void presence_charge_start()
{
  // Disable pin change interrupt on PB1.
  // PCMSK &= ~(1 << PCINT1);

  // Configure PB1 to output high.
  PORTB |= (1 << PORTB1);
  DDRB |= (1 << DDB1);

  // Wait until the capacitor is charged.
  while (!pin_presence())
    ;

  // Re-enable pin change interrupt on PB1.
  // This has been commented out because it screwed with the timing (well,
  // mostly letting the uC sleep and waking it up on pin change did).
  // PCMSK |= 1 << PCINT1;

  // Configure PB1 as tri-state input.
  DDRB &= ~(1 << DDB1);     // first as pull-up input
  PORTB &= ~(1 << PORTB1);  // disable pull-up
}

bool presence_is_discharged()
{
  if (pin_presence()) {
    // Still high, have to wait longer.
    return false;
  }

  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    discharge_count++;
  }
  return true;
}

uint16_t presence_discharge_count()
{
  uint16_t count;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    count = discharge_count;
    discharge_count = 0;
  }
  return count;
}

bool presence_tick()
{
  if (presence_is_discharged()) {
    presence_charge_start();
  }

  static bool is_touching = false;
  static uint16_t last_presence = 0;
  if (!timekeeping_ms_periodic(&last_presence, 20)) {
    return is_touching;
  }

  const int16_t discharges = presence_discharge_count();

  static float threshold = 0;
  static uint8_t same_state_cycles = 0;
  bool was_touching = is_touching;

  is_touching = discharges < threshold;

  if (is_touching) {
    // if (same_state_cycles > 50)
    //   threshold += (discharges - threshold) / 20;
  }
  else {
    if (threshold == 0)
      threshold = discharges;
    else if (same_state_cycles > 50)
      threshold += (0.85f * discharges - threshold) / 20;
  }

  if (was_touching == is_touching) {
    if (same_state_cycles < 255)
      same_state_cycles++;
  }
  else {
    same_state_cycles = 0;
  }

  return is_touching;
}
