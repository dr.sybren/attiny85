/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#include <stdint.h>

void servo_setup(void);
void servo_tick(void);

#define SERVO_LID 0
#define SERVO_PAW 1
void servo_set_position(uint8_t servo, float position, uint8_t speed);

extern volatile uint16_t servo_last_update_time;
// Time in milliseconds after which the servo is considered to be at its position.
#define SERVO_LAG_TIME 2000
