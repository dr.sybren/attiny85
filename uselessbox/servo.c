/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "servo.h"
#include "hardware.h"
#include "timekeeping.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#if F_CPU == 20000000
#  define SERVO_TICKS_MIN 39
#  define SERVO_TICKS_MAX 195
#else
#  warning Unsupported F_CPU
#  define SERVO_TICKS_MIN 39
#  define SERVO_TICKS_MAX 195
#endif

#define SERVO_TICKS_MID ((SERVO_TICKS_MIN + SERVO_TICKS_MAX) >> 1)
#define SERVO_TIMER_CLKDIV (0b1001 << CS10)
#define SERVO_TIMER_CLKDIV_MASK (0b1111 << CS10)

volatile uint16_t servo_last_update_time;

typedef struct {
  uint8_t pin;
  uint8_t ticks;
  uint8_t current_pos;
  uint8_t speed;
} ServoStatus;

#define SERVO_COUNT 2
volatile ServoStatus servo_status[SERVO_COUNT];
volatile uint8_t servo_index;

void servo_setup(void)
{
  servo_status[SERVO_LID].pin = PORTB3;
  servo_status[SERVO_PAW].pin = PORTB4;
  servo_status[SERVO_LID].ticks = SERVO_TICKS_MID;
  servo_status[SERVO_PAW].ticks = SERVO_TICKS_MID;
  servo_status[SERVO_LID].current_pos = SERVO_TICKS_MID;
  servo_status[SERVO_PAW].current_pos = SERVO_TICKS_MID;
  servo_status[SERVO_LID].speed = 1;
  servo_status[SERVO_PAW].speed = 1;
  servo_index = 0;

  // Set PB0 and PB1 as low outputs.
  PORTB &= ~(0b11 << PORTB0);
  DDRB |= (1 << DDB0) | (1 << DDB1);

  // Use Timer 1 for pulse durations.
  // T1 was chosen because it has more clockdiv options than T0.
  // It needs to be able to time intervals up to 2500 us.
  // Timer 1 for keeping track of time at 1 kHz.
  TCNT1 = 0;
  OCR1A = OCR1C = SERVO_TICKS_MID;
  TCCR1 = 0                 //
          | 1 << CTC1       // CTC mode
          | 0b0000 << CS10  // T0 stopped initially
      ;
  TIMSK |= (1 << OCIE1A);  // Interrupt on compare with OCR1A
}

void servo_set_position(const uint8_t servo, const float position, const uint8_t speed)
{
  if (position < 0 || position > 1) {
    return;
  }
  servo_last_update_time = timekeeping_ms_since_reset();

  const uint8_t ticks = (position * (SERVO_TICKS_MAX - SERVO_TICKS_MIN)) + SERVO_TICKS_MIN;
  servo_status[servo].ticks = ticks;
  servo_status[servo].speed = speed;
}

inline int16_t abslimit(const int16_t value, const int16_t limit)
{
  if (value < -limit)
    return -limit;
  if (value > limit)
    return limit;
  return value;
}

void servo_tick(void)
{
  static uint16_t last_time_ms = 0;
  // The time to wait is 20ms / SERVO_COUNT.
  if (!timekeeping_ms_periodic(&last_time_ms, 20)) {
    return;
  }

  // Move to the next servo.
  servo_index = (servo_index + 1) % SERVO_COUNT;

  volatile ServoStatus *servo = &servo_status[servo_index];
  const int16_t diff = servo->ticks - servo->current_pos;
  servo->current_pos += abslimit(diff, servo->speed);

  PORTB |= (1 << servo->pin);

  // Turn on T1 and count from the start.
  TCNT1 = 0;
  OCR1A = OCR1C = servo->current_pos;
  TCCR1 |= SERVO_TIMER_CLKDIV;
}

ISR(TIMER1_COMPA_vect)
{
  const uint8_t sreg = SREG;

  PORTB &= ~(1 << servo_status[servo_index].pin);

  // Turn off timer 1 until the next servo_tick().
  TCCR1 &= SERVO_TIMER_CLKDIV_MASK;
  TCNT1 = 0;

  SREG = sreg;
}
