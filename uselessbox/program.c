/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "program.h"
#include "pawlid.h"
#include "servo.h"
#include "timekeeping.h"

#include <avr/pgmspace.h>
#include <stddef.h>

typedef struct ProgramStep {
  uint16_t time_ms;
  uint8_t servo;
  float position;
  uint8_t speed;
} ProgramStep;

typedef ProgramStep Program[];

static Program _program_attack0 = {
    // time_ms, servo, position, speed
    {150, SERVO_LID, 0.15f, 1},
    {400, SERVO_LID, 0.3f, 3},
    {200, SERVO_PAW, 0.5f, 3},
    {500, SERVO_PAW, 0.0f, 4},
    {600, SERVO_LID, 1.0f, 100},
    {200, SERVO_PAW, 1.0f, 100},
    {0, 0, 0, 0},
};
static Program _program_release0 = {
    // time_ms, servo, position, speed
    {0, SERVO_LID, 0.5f, 3},
    {100, SERVO_PAW, 0.0f, 5},
    {600, SERVO_LID, 0.0f, 100},
    {0, 0, 0, 0},
};

static Program _program_attack1 = {
    // time_ms, servo, position, speed
    {100, SERVO_LID, 0.05f, 1},
    {100, SERVO_LID, 0.10f, 1},
    {100, SERVO_LID, 0.15f, 1},
    {100, SERVO_LID, 0.20f, 1},
    {1000, SERVO_LID, 1.0f, 100},
    {300, SERVO_PAW, 1.0f, 100},
    {0, 0, 0, 0},
};
static Program _program_release1 = {
    // time_ms, servo, position, speed
    {0, SERVO_PAW, 0.0f, 100},
    {100, SERVO_LID, 0.0f, 100},
    {0, 0, 0, 0},
};

static Program _program_attack2 = {
    // time_ms, servo, position, speed
    {100, SERVO_LID, 0.2f, 2},
    {300, SERVO_LID, 0.0f, 100},
    {100, SERVO_LID, 0.2f, 100},
    {300, SERVO_LID, 0.0f, 100},
    {100, SERVO_LID, 0.2f, 100},
    {300, SERVO_LID, 0.0f, 100},
    {1000, SERVO_LID, 0.4f, 100},
    {100, SERVO_PAW, 1.0f, 100},
    {0, 0, 0, 0},
};
static Program _program_release2 = {
    // time_ms, servo, position, speed
    {0, SERVO_PAW, 0.85f, 1},
    {200, SERVO_PAW, 0.70f, 1},
    {200, SERVO_PAW, 0.55f, 1},
    {300, SERVO_PAW, 0.0f, 100},
    {100, SERVO_LID, 0.0f, 100},
    {0, 0, 0, 0},
};

static Program *attacks[] = {&_program_attack0, &_program_attack1, &_program_attack2};
static Program *releases[] = {&_program_release0, &_program_release1, &_program_release2};
static uint8_t num_attacks = sizeof(attacks) / sizeof(attacks[0]);
static uint8_t num_releases = sizeof(releases) / sizeof(releases[0]);

static uint16_t program_start_time_ms;
static uint8_t current_step, current_behaviour_idx;
static Program *curprog;

static void make_cumulative(Program *program);

void program_setup()
{
  current_behaviour_idx = 0;
  current_step = 0;
  curprog = NULL;
  program_start_time_ms = 0;

  for (uint8_t i = 0; i < num_attacks; i++) {
    make_cumulative(attacks[i]);
  }
  for (uint8_t i = 0; i < num_releases; i++) {
    make_cumulative(releases[i]);
  }
}

// Turn delays into absolute times.
static void make_cumulative(Program *program)
{
  uint16_t cumulative_time = 0;
  for (uint8_t step = 0;; ++step) {
    ProgramStep *step_data = &(*program)[step];
    if (step_data->speed == 0)
      break;
    cumulative_time += step_data->time_ms;
    step_data->time_ms = cumulative_time;
  }
}

void program_start(const ProgramType program_type)
{
  switch (program_type) {
    case PROGRAM_TYPE_ATTACK:
      curprog = attacks[current_behaviour_idx];
      break;
    case PROGRAM_TYPE_RELEASE:
      curprog = releases[current_behaviour_idx];
      break;
    default:
      curprog = NULL;
      return;
  }
  program_start_time_ms = timekeeping_ms_since_reset();
  current_step = 0;

  current_behaviour_idx = (current_behaviour_idx + 1) % num_attacks;
}

bool program_tick()
{
  if (curprog == NULL)
    return false;
  const ProgramStep *step = &(*curprog)[current_step];
  if (step->speed == 0)
    return false;

  if (!timekeeping_ms_passed(program_start_time_ms, step->time_ms)) {
    // Still waiting for the current step.
    servo_tick();
    return true;
  }

  // Execute this step.
  switch (step->servo) {
    case SERVO_PAW:
      move_paw(step->position, step->speed);
      break;
    case SERVO_LID:
      move_lid(step->position, step->speed);
      break;
  }
  servo_tick();

  current_step++;
  return (*curprog)[current_step].speed != 0;
}
