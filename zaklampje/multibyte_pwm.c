#include "adc.h"
#include "multibyte_pwm.h"
#include "utildefs.h"

#include <stddef.h>
#include <avr/io.h>
#include <util/atomic.h>

volatile int16_t pwm_dac_values[2];
volatile uint8_t* pwm_ports[] = { &OCR0A, &OCR0B };
volatile uint8_t multibyte_pwm_cycle = 0;

float smoothed_input = 0.f;

static void setup()
{
  OCR0A = 255;
  OCR0B = 255;

  GTCCR = 1 << TSM  // Enable Timer Synchronisation Mode
    | 1 << PSR0;    // Reset Timer 0 counter

  // Pins PB0 (OC0A), PB1 (OC0B) get PWM.
  TCCR0A = 3 << WGM00  // fast PWM
    | 0b11 << COM0A0   // inverted PWM on OC0A
    | 0b11 << COM0B0;  // inverted PWM on OC0B
  TCCR0B = 0 << WGM02  // normal fast PWM
    | 0b010 << CS00;   // clock prescaler

  TIMSK = 1 << TOIE0;  // enable timer0 overflow interrupt

  TCCR1 = 0;  // Timer 1 disabled
  GTCCR = 0;

  // Set LED pins as output
  DDRB = 1 << DDB0 | 1 << DDB1;
}

static void enter_state()
{
  smoothed_input = 0.f;
}

static void iteration()
{
  if (multibyte_pwm_cycle > 0) return;

  uint16_t last_adc = adc_read();

  float input = ((float)last_adc) / 1023.f;
  float step_size = (input - smoothed_input) / 32.f;
  smoothed_input = min(max(0.f, smoothed_input+step_size), 1.f);

  uint16_t pwm = smoothed_input * PWM_MAX;
  multibyte_pwm_write(0, pwm);
  multibyte_pwm_write(1, pwm);
}

static void timer0_overflow_vector()
{
  static uint16_t remain_in_chan[2];
  uint16_t remain;

  for (uint8_t chan=0; chan<2; chan++) {
    if (multibyte_pwm_cycle == 0) {
      remain = pwm_dac_values[chan];
    }
    else {
      remain = remain_in_chan[chan];
    }

    if (remain > 255) {
      *pwm_ports[chan] = 0;  // inverted
      remain -= 256;
    }
    else {
      *pwm_ports[chan] = 255 - remain;  // inverted
      remain = 0;
    }
    remain_in_chan[chan] = remain;
  }

  multibyte_pwm_cycle = (multibyte_pwm_cycle + 1) & PWM_CYCLE_MASK;
}

State multibyte_pwm_state()
{
  State state = {
    .setup = setup,
    .enter_state = enter_state,
    .exit_state = NULL,
    .timer0_overflow_vector = timer0_overflow_vector,
    .iteration = iteration,
  };

  return state;
}

void multibyte_pwm_write(uint8_t channel, int16_t value)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    pwm_dac_values[channel] = value;
  }
}
