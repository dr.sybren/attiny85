#include "adc.h"
#include "blink.h"
#include "multibyte_pwm.h"
#include "strobe.h"
#include "utildefs.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

const uint8_t CLKPR_BOOT = 0b0010;

State state_blink;
State state_default;
State state_strobe;

void setup()
{
  PORTB = 0;
  PINB = 0;

  // Set system clock prescaler.
  CLKPR = 0x80;
  CLKPR = CLKPR_BOOT;

  // Enable pull-up on input pins
  PORTB = 1 << PORTB2 | 1 << PORTB4;

  adc_setup();

  state_blink = blink_state();
  if (state_blink.setup) state_blink.setup();

  state_strobe = strobe_state();
  if (state_strobe.setup) state_strobe.setup();

  state_default = multibyte_pwm_state();
  if (state_default.setup) state_default.setup();

  current_state = &state_default;

  // Disable digital pin change interrupts.
  GIMSK = 0;
  PCMSK = 0;
}

// Timer overflow
ISR(TIMER0_OVF_vect) {
  if (current_state->timer0_overflow_vector)
    current_state->timer0_overflow_vector();
}

void handle_user_input()
{
  if ((PINB & (1 << PINB2)) == 0) {
    state_switch(&state_blink);
  }
  else if ((PINB & (1 << PINB4)) == 0) {
    state_switch(&state_strobe);
  }
  else {
    state_switch(&state_default);
  }
}

int main()
{
  setup();
  sei();

  for(;;) {
    handle_user_input();

    if (current_state->iteration)
      current_state->iteration();

    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
}
