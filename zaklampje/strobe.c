#include "adc.h"
#include "multibyte_pwm.h"
#include "strobe.h"

#include <avr/io.h>
#include <stddef.h>
#include <util/atomic.h>

uint8_t clkpr;
uint8_t tccr0a;
uint8_t desired_pwm, even_odd;

static void enter_state()
{
  OCR0A = OCR0B = desired_pwm = 0;
  even_odd = 0;

  ATOMIC_BLOCK(ATOMIC_FORCEON) {
    clkpr = CLKPR;
    tccr0a = TCCR0A;

    TCCR0A = 3 << WGM00  // fast PWM
      | 0b10 << COM0A0   // normal PWM on OC0A
      | 0b10 << COM0B0;  // normal PWM on OC0B

    CLKPR = 1 << CLKPCE;
    CLKPR = 0b1000;
  }
}

static void exit_state()
{
  OCR0A = OCR0B = 255;

  ATOMIC_BLOCK(ATOMIC_FORCEON) {
    CLKPR = 1 << CLKPCE;
    CLKPR = clkpr;
    TCCR0A = tccr0a;
  }
}

static void timer0_overflow_vector()
{
  even_odd = 1 - even_odd;
  if (even_odd) {
    OCR0A = OCR0B = 0;
  }
  else {
    OCR0A = OCR0B = desired_pwm;
  }
}

static void iteration()
{
  desired_pwm = adc_read() >> 2;
}

State strobe_state()
{
  State state = {
    .setup = NULL,
    .enter_state = enter_state,
    .exit_state = exit_state,
    .timer0_overflow_vector = timer0_overflow_vector,
    .iteration = iteration,
  };

  return state;
}
