#include "adc.h"

#include <stdbool.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>


void adc_setup()
{
  ADCSRA = 1 << ADEN  // enable ADC
    | 0b011 << ADPS0; // clock divider

  ADMUX = 0b000 << REFS0  // Vcc as Vref
    | 0b0011 << MUX0;     // read from PB3
}

uint16_t adc_read()
{
  // A single conversion is started by writing a logical one to the ADC Start Conversion bit, ADSC.
  ADCSRA |= 1 << ADSC | 1 << ADIE;

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();

  while (ADCSRA & (1 << ADSC)) _delay_us(10);
  return ADC;
}

ISR(ADC_vect){}
