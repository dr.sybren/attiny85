#include "state_machine.h"
#include <stddef.h>

volatile State *current_state = NULL;

void state_switch(State *new_state)
{
  if (current_state == new_state) return;

  if (current_state && current_state->exit_state) {
    current_state->exit_state();
  }

  current_state = new_state;

  if (current_state && current_state->enter_state) {
    current_state->enter_state();
  }
}
