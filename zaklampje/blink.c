#include "blink.h"
#include "multibyte_pwm.h"
#include "utildefs.h"
#include <stddef.h>

volatile uint8_t blink_timer_cycle = 0;

uint8_t blink_port = 0;
float blink_pwm = 0.f;
float blink_direction = 1;

static void state_enter()
{
  blink_pwm = 0.f;
  blink_direction = 1;
}

static void timer0_overflow_vector()
{
  blink_timer_cycle = (blink_timer_cycle + 1) & 0x3;
  if (blink_timer_cycle) return;

  float step_size = max(1.f, blink_pwm * blink_pwm / 4096.f);

  blink_pwm += blink_direction * step_size;
  if (blink_pwm > 255) {
    blink_direction = -1;
    blink_pwm = 255;
  }
  else if (blink_pwm < 0) {
    blink_direction = 1;
    blink_pwm = 0;
    blink_port = 1 - blink_port;
  }

  *pwm_ports[blink_port] = 255-blink_pwm;
  *pwm_ports[1-blink_port] = 255;
}


State blink_state()
{
  State state = {
    .setup = NULL,
    .enter_state = state_enter,
    .exit_state = NULL,
    .timer0_overflow_vector = timer0_overflow_vector,
    .iteration = NULL,
  };

  return state;
}
