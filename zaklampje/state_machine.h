#pragma once

typedef struct {
  void (*setup)();
  void (*enter_state)();
  void (*exit_state)();
  void (*timer0_overflow_vector)();
  void (*iteration)();
} State;

extern volatile State *current_state;
void state_switch(State *new_state);
