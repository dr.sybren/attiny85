#pragma once
#include "state_machine.h"
#include <inttypes.h>

#define PWM_BITS 10
#define PWM_CYCLE_MASK ((1 << (PWM_BITS-8)) - 1)
#define PWM_MAX ((1 << PWM_BITS) - 1)

extern volatile uint8_t* pwm_ports[2];
extern volatile uint8_t multibyte_pwm_cycle;

State multibyte_pwm_state();

void multibyte_pwm_write(uint8_t channel, int16_t value);
