#pragma once
#include <inttypes.h>
#include "state_machine.h"

extern volatile uint8_t blink_timer_cycle;

State blink_state();
