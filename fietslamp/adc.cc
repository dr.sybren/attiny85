/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "adc.h"

#include <avr/interrupt.h>

void adc_setup(void)
{
  ADMUX = 0                 //
          | 0b11 << REFS0   // 2.56v as voltage ref + cap on PB0/AREF
          | 1 << ADLAR      // Left-align
          | 1 << REFS2      // 2.56v as voltage ref + cap on PB0/AREF
          | 0b0000 << MUX0  // Read from ADC0 / PB5
      ;
  ADCSRA = 0                 //
           | 1 << ADEN       // Enable ADC
           | 1 << ADATE      // Auto-trigger enable
           | 1 << ADIE       // interrupt required for triggering free-running mode
           | 0b100 << ADPS0  // scale ADC clock so it's 50-200 KHz
      ;
  ADCSRB = 0                 //
           | 0b000 << ADTS0  // Auto-trigger source
      ;
  DIDR0 = 0             //
          | 1 << ADC0D  // disable digital input on ADC0 / PB5
      ;

  // Trigger a conversion to start freerunning mode.
  ADCSRA |= 1 << ADSC;
}

ISR(ADC_vect)
{
}

uint8_t adc_battery_level()
{
  const uint8_t adc = ADCH;

  // Vref = 2.56 volt, so 0.01 volt per ADC level.
  // Battery voltage is fed through 1:2 divider.
  // Low  = 3.2v -> 1.6v -> ADC=160
  // High = 4.2v -> 2.1v -> ADC=210

  constexpr uint8_t ADC_LOW = 180;  // slightly different because of non-ideal voltage divider.
  constexpr uint8_t ADC_HIGH = 210;

  if (adc <= ADC_LOW)
    return 1;
  if (adc >= ADC_HIGH)
    return 12;

  constexpr uint8_t input_range = ADC_HIGH - ADC_LOW;
  constexpr uint8_t output_range = 12;

  const uint8_t level = (adc - ADC_LOW) * output_range / input_range + 1;
  return level;
}
