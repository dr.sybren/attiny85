/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdbool.h>

#include <avr/io.h>

/* Pinout:
 * PB0: AREF capacitor to ground
 * PB1: OUT Latch Data
 * PB2: OUT Serial Clock
 * PB3: OUT Serial Data
 * PB4: OUT Blank
 * PB5: AIN Battery voltage
 */

void pins_setup();

// Write to PB1
static inline void pin_write_latch(bool pin_state)
{
  if (pin_state) {
    PORTB |= 1 << PORTB1;
  }
  else {
    PORTB &= ~(1 << PORTB1);
  }
}

// Write to PB2
static inline void pin_write_serial_clock(bool pin_state)
{
  if (pin_state) {
    PORTB |= 1 << PORTB2;
  }
  else {
    PORTB &= ~(1 << PORTB2);
  }
}

// Write to PB3
static inline void pin_write_serial_data(bool pin_state)
{
  if (pin_state) {
    PORTB |= 1 << PORTB3;
  }
  else {
    PORTB &= ~(1 << PORTB3);
  }
}

// Write to PB4
static inline void pin_write_blank(bool pin_state)
{
  if (pin_state) {
    PORTB |= 1 << PORTB4;
  }
  else {
    PORTB &= ~(1 << PORTB4);
  }
}
