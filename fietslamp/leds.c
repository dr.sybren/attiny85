/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "leds.h"
#include "pins.h"

#ifdef LEDS_INDIVIDUAL_PWM
#  include "random.h"
#endif

#include <avr/pgmspace.h>

void leds_setup()
{
  pin_write_blank(true);
  leds_write_16bit(0);
  pin_write_blank(false);

  TCCR1 = 0                 //
          | 0b0100 << CS10  // clock select
      ;
  GTCCR = 0                 //
          | 1 << PWM1B      // enable PWM 1B
          | 0b10 << COM1B0  // output to OC1B/PB4
      ;
  OCR1B = 16;
}

static const uint8_t light_map_8bit[] PROGMEM = {
    255, 254, 253, 251, 248, 243, 236, 225, 208, 183, 145, 88, 0};
void leds_set_pwm(const uint8_t light_level)
{
  OCR1B = pgm_read_byte(&light_map_8bit[light_level]);
}

void leds_write_16bit(uint16_t bit_pattern)
{
  for (uint8_t bit_idx = 0; bit_idx < 16; bit_idx++) {
    const bool bit = (bit_pattern & 1);
    pin_write_serial_data(bit);
    pin_write_serial_clock(true);
    bit_pattern >>= 1;
    pin_write_serial_clock(false);
  }
  pin_write_latch(true);
  pin_write_latch(false);
}

static const uint16_t progress_bits[] PROGMEM = {
    0b0000000000000000,  // 0
    0b0000000000000001,  // 1
    0b0000000000000011,  // 2
    0b0000000000000111,  // 3
    0b0000000000001111,  // 4
    0b0000000010001111,  // 5
    0b0000100010001111,  // 6
    0b1000100010001111,  // 7
    0b1100100010001111,  // 8
    0b1110100010001111,  // 9
    0b1111100010001111,  // 10
    0b1111100110001111,  // 11
    0b1111100110011111,  // 12
};
void leds_progress_bar(const uint8_t progress)  // values 0 (off) to 12 (full)
{
  const uint16_t bit_pattern = pgm_read_word(&progress_bits[progress]);
  leds_write_16bit(bit_pattern);
}

#ifdef LEDS_INDIVIDUAL_PWM
uint8_t leds_level[16];
void leds_levels_update()
{
  uint16_t bit_pattern = 0;

  const uint8_t random_value = random_byte();
  const uint8_t *current_level = leds_level;
  for (int8_t led_index = 0; led_index < 16; ++led_index, ++current_level) {
    const bool led_on = *current_level > random_value;
    bit_pattern |= (led_on << led_index);
  }

  leds_write_16bit(bit_pattern);
}
#endif
