#include "adc.h"
#include "leds.h"
#include "pins.h"
#include "random.h"
#include "timekeeping.h"

#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

static void battery_measure_at_boot();
static void fade_to_white();

int main(void)
{
  // random_setup();
  pins_setup();
  leds_setup();
  adc_setup();
  timer_setup();

  sei();

  battery_measure_at_boot();
  fade_to_white();

  const uint8_t led_seq[] = {0, 1, 2, 3, 7, 6, 5, 4, 8, 9, 10, 11, 15, 14, 13, 12, 8, 4};
  const uint8_t led_seq_len = sizeof(led_seq) / sizeof(led_seq[0]);
  uint8_t led_seq_idx1 = 0;
  uint8_t led_seq_idx2 = led_seq_len / 3;

  for (;;) {
    timer_wait_ticks(8);

    const uint8_t battery_level = adc_battery_level();
    if (battery_level < 3) {
      leds_set_pwm(4);
      continue;
    }

    // // OR two bitpatterns together to get an ON:OFF ratio of 75:25%.
    // const uint16_t bit_pattern = random_word() | random_word();

    const uint16_t bit_pattern = ~((1 << led_seq[led_seq_idx1]) | (1 << led_seq[led_seq_idx2]));
    leds_write_16bit(bit_pattern);

    led_seq_idx1 = (led_seq_idx1 + 1) % led_seq_len;
    led_seq_idx2 = (led_seq_idx2 + 1) % led_seq_len;
  }

  return 0;
}

static void battery_measure_at_boot()
{
  for (uint8_t counter = 0; counter < 13; ++counter) {
    leds_progress_bar(counter);
    leds_set_pwm(counter);
    timer_wait_ticks(4);
  }
  timer_wait_ticks(4);
  const uint8_t battery_level = adc_battery_level();
  for (uint8_t counter = 11; counter > battery_level; --counter) {
    leds_progress_bar(counter);
    timer_wait_ticks(4);
  }

  for (uint8_t counter = 0; counter < 40; ++counter) {
    const uint8_t battery_level = adc_battery_level();
    leds_progress_bar(battery_level);
    timer_wait_ticks(4);
  }

  for (uint8_t counter = 12; counter > 0; --counter) {
    leds_set_pwm(counter);
    timer_wait_ticks(2);
  }
}

static void fade_to_white()
{
  leds_set_pwm(0);
  leds_write_16bit(~0);
  for (uint8_t counter = 0; counter < 13; ++counter) {
    leds_set_pwm(counter);
    timer_wait_ticks(2);
  }
}
