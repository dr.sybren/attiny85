/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2022 Sybren A. Stüvel.
 */
#include "driver_pca9624.h"
#include "hardware.h"
#include "i2c_attiny.h"

#include <math.h>
#include <stdbool.h>
#include <stdint.h>

static uint8_t pca9624_i2c_addr = 0x18;

void pca9624_init(uint8_t i2c_addr)
{
  pca9624_i2c_addr = i2c_addr;
}

void pca9624_software_reset()
{
  i2c_start();
  i2c_write_address(PCA9624_ADDR_SWRST, I2C_WRITE);
  i2c_write_byte(0xA5);
  i2c_write_byte(0x5A);
  i2c_stop();
}

void pca9624_write(const uint8_t regaddr, const uint8_t value)
{
  i2c_start();
  i2c_write_address(pca9624_i2c_addr, I2C_WRITE);
  i2c_write_byte(regaddr);
  i2c_write_byte(value);
  i2c_stop();
}

void pca9624_start(const uint8_t regaddr)
{
  i2c_start();
  i2c_write_address(pca9624_i2c_addr, I2C_WRITE);
  i2c_write_byte(regaddr);
}

void pca9624_stop()
{
  i2c_stop();
}

uint8_t pca9624_read(const uint8_t regaddr)
{
  i2c_start_address(pca9624_i2c_addr, I2C_READ);
  i2c_write_byte(regaddr);
  const uint8_t value = i2c_read_byte(I2C_NACK);
  i2c_stop();
  return value;
}

void pca9624_set_mode1(const PCA9624_reg_mode1 value)
{
  pca9624_write(PCA9624_REG_MODE1, value.value);
}

void pca9624_set_mode2(const PCA9624_reg_mode2 value)
{
  pca9624_write(PCA9624_REG_MODE2, value.value);
}

void pca9624_set_ledout(const uint8_t regaddr, const PCA9624_reg_ledout value)
{
  pca9624_write(regaddr, value.value);
}

void pca9624_set_address(const uint8_t regaddr, const PCA9624_reg_address value)
{
  pca9624_write(PCA9624_REG_LEDOUT1, value.value);
}
