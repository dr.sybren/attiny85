/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2022 Sybren A. Stüvel.
 */
#pragma once
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* PCA9624 8-channel, 8-bit LED driver. */

typedef enum PCA9624_default_address {
  PCA9624_ADDR_ALLCALL = 0xE0, /* LED "all-call" I2C-bus address */
  PCA9624_ADDR_SUB1 = 0xE2,    /* LED Sub Call I2C-bus address 1 */
  PCA9624_ADDR_SUB2 = 0xE4,    /* LED Sub Call I2C-bus address 2 */
  PCA9624_ADDR_SUB3 = 0xE8,    /* LED Sub Call I2C-bus address 3 */
  PCA9624_ADDR_SWRST = 0x03,   /* Software Reset I2C-bus address */
} PCA9624_default_address;

// OR these with the register address to set the auto-increment mode.
typedef enum PCA9624_auto_increment {
  PCA9624_AI_NONE = 0b00000000,              /* No auto-increment (AI) */
  PCA9624_AI_ALL_REG = 0b10000000,           /* AI for all registers */
  PCA9624_AI_INDIV_BRIGHT = 0b10100000,      /* AI for individual brightness */
  PCA9624_AI_GLOBAL_CTRL = 0b11000000,       /* AI for global control registers */
  PCA9624_AI_INDIV_GLOBAL_CTRL = 0b11100000, /* AI for individual and global control registers */
} PCA9624_auto_increment;

typedef enum PCA9624_register {
  PCA9624_REG_MODE1 = 0x00,      /* Mode register 1 */
  PCA9624_REG_MODE2 = 0x01,      /* Mode register 2 */
  PCA9624_REG_PWM0 = 0x02,       /* brightness control LED0 */
  PCA9624_REG_PWM1 = 0x03,       /* brightness control LED1 */
  PCA9624_REG_PWM2 = 0x04,       /* brightness control LED2 */
  PCA9624_REG_PWM3 = 0x05,       /* brightness control LED3 */
  PCA9624_REG_PWM4 = 0x06,       /* brightness control LED4 */
  PCA9624_REG_PWM5 = 0x07,       /* brightness control LED5 */
  PCA9624_REG_PWM6 = 0x08,       /* brightness control LED6 */
  PCA9624_REG_PWM7 = 0x09,       /* brightness control LED7 */
  PCA9624_REG_GRPPWM = 0x0A,     /* group duty cycle control */
  PCA9624_REG_GRPFREQ = 0x0B,    /* group frequency */
  PCA9624_REG_LEDOUT0 = 0x0C,    /* LED output state 0 */
  PCA9624_REG_LEDOUT1 = 0x0D,    /* LED output state 1 */
  PCA9624_REG_SUBADR1 = 0x0E,    /* I2C-bus subaddress 1 */
  PCA9624_REG_SUBADR2 = 0x0F,    /* I2C-bus subaddress 2 */
  PCA9624_REG_SUBADR3 = 0x10,    /* I2C-bus subaddress 3 */
  PCA9624_REG_ALLCALLADR = 0x11, /* LED All Call I2C-bus address */
} PCA9624_register;

typedef union PCA9624_reg_mode1 {
  struct {
    // * = default value.

    // 0: PCA9624 does not respond to LED All Call I2C-bus address.
    // 1* PCA9624 responds to LED All Call I2C-bus address.
    uint8_t allcall : 1; /* Bit 0 */

    // 0* PCA9624 does not respond to I 2C-bus subaddress 3.
    // 1: PCA9624 responds to I2C-bus subaddress 3.
    uint8_t sub3 : 1; /* Bit 1 */

    // 0* PCA9624 does not respond to I 2C-bus subaddress 2.
    // 1: PCA9624 responds to I2C-bus subaddress 2.
    uint8_t sub2 : 1; /* Bit 2 */

    // 0* PCA9624 does not respond to I 2C-bus subaddress 1.
    // 1: PCA9624 responds to I2C-bus subaddress 1.
    uint8_t sub1 : 1; /* Bit 3 */

    // 0: Normal mode[2].
    // 1* Low-power mode. Oscillator off[3].

    // This bit must be programmed with logic 0 for proper device operation.
    // No blinking or dimming is possible when the oscillator is off
    //
    // It takes 500 us max for the oscillator to be up and running once SLEEP
    // bit has been set to logic 0. Timings on LEDn outputs are not guaranteed
    // if PWMx, GRPPWM or GRPFREQ registers are accessed within the 500 us
    // window.
    uint8_t sleep : 1; /* Bit 4 */

    // Auto-increment fields are read-only.
    // Auto-Increment bits 0 and 1, default = 0b00.
    uint8_t auto_increment_mode : 2; /* Bit 5-6 */

    // 0: Register Auto-Increment disabled.
    // 1* Register Auto-Increment enabled.
    uint8_t auto_increment_enabled : 1; /* Bit 7 */
  };
  uint8_t value;
} PCA9624_reg_mode1;

typedef union PCA9624_reg_mode2 {
  struct {
    // * = default value.

    uint8_t reserved_rw_0 : 1; /* Bit 0, reserved, write must be 1 */
    uint8_t reserved_rw_1 : 1; /* Bit 1, reserved, write must be 0 */
    uint8_t reserved_rw_2 : 1; /* Bit 2, reserved, write must be 1 */

    // 0* outputs change on STOP command [1]
    // 1: outputs change on ACK
    uint8_t stop_ack : 1; /* Bit 3 */

    uint8_t invert : 1; /* Bit 4, reserved, write must be 0 */

    // 0* group control = dimming.
    // 1: group control = blinking.
    uint8_t dim_blink : 1; /* Bit 5 */

    uint8_t reserved_ro_6 : 1; /* Bit 6, reserved, write is ignored */
    uint8_t reserved_ro_7 : 1; /* Bit 7, reserved, write is ignored */
  };
  uint8_t value;
} PCA9624_reg_mode2;

// LED drive mode, used in PCA9624_reg_ledout register.
typedef enum PCA9624_ldr {
  PCA9624_LDR_OFF = 0b00,   /* LEDx is off. */
  PCA9624_LDR_ON = 0b01,    /* LEDx is fully on. */
  PCA9624_LDR_PWM = 0b10,   /* LEDx brightness from individual PWM only. */
  PCA9624_LDR_GROUP = 0b11, /* LEDx individual PWM + group PWM/blinking. */
} PCA9624_ldr;

// Values for PCA9624_REG_LEDOUT[0:1]
typedef union PCA9624_reg_ledout {
  struct {
    PCA9624_ldr ldr_0_4 : 2; /* Bit 0-1, LDR0 (in LEDOUT0) / LDR4 (in LEDOUT1) */
    PCA9624_ldr ldr_1_5 : 2; /* Bit 2-3, LDR1 (in LEDOUT0) / LDR5 (in LEDOUT1) */
    PCA9624_ldr ldr_2_6 : 2; /* Bit 4-5, LDR2 (in LEDOUT0) / LDR6 (in LEDOUT1) */
    PCA9624_ldr ldr_3_7 : 2; /* Bit 6-7, LDR3 (in LEDOUT0) / LDR7 (in LEDOUT1) */
  };
  uint8_t value;
} PCA9624_reg_ledout;

// Sub-address, for registers PCA9624_REG_ALLCALLADR and PCA9624_REG_SUBADR[1:3]
typedef union PCA9624_reg_address {
  struct {
    uint8_t reserved : 1; /* Bit 0, must be written 0 */
    uint8_t address : 7;  /* Bit 1-7 */
  };
  uint8_t value;
} PCA9624_reg_address;

void pca9624_init(uint8_t i2c_addr);

void pca9624_software_reset();

void pca9624_write(uint8_t regaddr, uint8_t value);
uint8_t pca9624_read(uint8_t regaddr);

void pca9624_start(uint8_t regaddr);
void pca9624_stop();

void pca9624_set_mode1(PCA9624_reg_mode1 value);
void pca9624_set_mode2(PCA9624_reg_mode2 value);

void pca9624_set_ledout(uint8_t regaddr, PCA9624_reg_ledout value);
void pca9624_set_address(uint8_t regaddr, PCA9624_reg_address value);

#ifdef __cplusplus
}
#endif
