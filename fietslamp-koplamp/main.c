#include "adc.h"
#include "driver_pca9624.h"
#include "hardware.h"
#include "i2c_attiny.h"
#include "timekeeping.h"

#include <string.h>

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <util/delay.h>

#define LED_DRIVER_I2C_ADDR 0x00

void reboot();
void led_driver_init();
void sleepytime();
void battery_measure_at_boot();
uint8_t battery_measure();
void leds_progress_bar(uint8_t progress);  // values 0 (off) to 8 (full)

void boot();

typedef struct Mode {
  void (*start)();
  bool (*tick)();  // Return whether the global `pwm_values` was changed.
} Mode;

void mode_battery_low_start();
bool mode_battery_low_tick();
void mode_knight_rider_start();
bool mode_knight_rider_tick();
void mode_breathing_start();
bool mode_breathing_tick();

Mode mode_battery_low = {
    mode_battery_low_start,
    mode_battery_low_tick,
};
Mode mode_knight_rider = {
    mode_knight_rider_start,
    mode_knight_rider_tick,
};
Mode mode_breathing = {
    mode_breathing_start,
    mode_breathing_tick,
};

volatile int16_t last_time_button_press = 0;
volatile Mode *mode_current = &mode_breathing;

uint8_t update_timer = 0;
int8_t bright_led_index = 0;
int8_t bright_led_direction = 1;

#define NUM_LEDS 8
uint8_t pwm_values[NUM_LEDS];

int main(void)
{
  WDTCR |= 1 << WDCE | 1 << WDE;  // Permit changes.
  WDTCR = 0 << WDE;               // Disable watchdog.

  // Set clock prescaler to go to 2 MHz.
  CLKPR = 1 << CLKPCE;
  CLKPR = 0b0010 << CLKPS0;

  // Disable anything we're not using.
  PRR = 0              //
        | 1 << PRTIM1  // Timer 1
        | 1 << PRUSI   // Serial Bus (implemented via bit-banging instead)
      ;

  sei();

  adc_setup();
  hardware_setup();
  i2c_init();
  timekeeping_setup();

  GIMSK |= 1 << PCIE;
  PCMSK = 1 << PCINT4;  // Pin change interrupt on PB4 = pushbutton

  boot();

  bool brightness_changed = false;
  uint16_t battery_check_time = 0;
  for (;;) {
    brightness_changed = mode_current->tick();

    // Send brightnessesses to the LEDs.
    if (brightness_changed) {
      pca9624_start(PCA9624_REG_PWM0 | PCA9624_AI_INDIV_BRIGHT);
      for (GPIOR0 = 0; GPIOR0 < NUM_LEDS; ++GPIOR0) {
        i2c_write_byte(pwm_values[GPIOR0]);
      }
      pca9624_stop();
      brightness_changed = false;
    }

    if (timekeeping_ms_periodic(&battery_check_time, 1000)) {
      // Do a battery check.
      const uint8_t bat_level = battery_measure();
      if (bat_level < 2 && mode_current != &mode_battery_low) {
        mode_current = &mode_battery_low;
        mode_current->start();
      }
      else if (bat_level > 2 && mode_current == &mode_battery_low) {
        mode_current = &mode_breathing;
        mode_current->start();
      }
    }

    if (is_pushbutton_pressed() && timekeeping_ms_passed(last_time_button_press, 1000)) {
      sleepytime();
    }

    // Sleep until the timer wakes us up.
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
}

void boot()
{
  led_driver_init();
  battery_measure_at_boot();

  update_timer = 0;
  bright_led_index = 0;
  bright_led_direction = 1;

  memset(pwm_values, 255, NUM_LEDS);

  PCA9624_reg_ledout ledout = {0};
  ledout.ldr_0_4 = PCA9624_LDR_GROUP;
  ledout.ldr_1_5 = PCA9624_LDR_GROUP;
  ledout.ldr_2_6 = PCA9624_LDR_GROUP;
  ledout.ldr_3_7 = PCA9624_LDR_GROUP;
  pca9624_set_ledout(PCA9624_REG_LEDOUT0, ledout);
  pca9624_set_ledout(PCA9624_REG_LEDOUT1, ledout);

  mode_current = &mode_breathing;
  mode_current->start();
}

// Reboot the microcontroller via a watchdog reset.
void reboot()
{
  WDTCR |= 1 << WDCE | 1 << WDE;  // Permit changes.
  WDTCR = 1 << WDE;               // Enable at fastest speed.
  for (;;)
    ;
}

void led_driver_init()
{
  voltreg_enable(true);
  pca9624_software_reset();

  pca9624_init(LED_DRIVER_I2C_ADDR);
  PCA9624_reg_mode1 mode1 = {0};
  mode1.allcall = true;
  mode1.sub1 = false;
  mode1.sub2 = false;
  mode1.sub3 = false;
  mode1.sleep = false;
  mode1.auto_increment_enabled = true;
  pca9624_set_mode1(mode1);

  PCA9624_reg_ledout ledout = {0};
  ledout.ldr_0_4 = PCA9624_LDR_OFF;
  ledout.ldr_1_5 = PCA9624_LDR_OFF;
  ledout.ldr_2_6 = PCA9624_LDR_OFF;
  ledout.ldr_3_7 = PCA9624_LDR_OFF;
  pca9624_set_ledout(PCA9624_REG_LEDOUT0, ledout);
  pca9624_set_ledout(PCA9624_REG_LEDOUT1, ledout);

  pca9624_write(PCA9624_REG_GRPPWM, 16);

  // Send medium brightness to the LEDs
  pca9624_start(PCA9624_REG_PWM0 | PCA9624_AI_INDIV_BRIGHT);
  for (GPIOR0 = 0; GPIOR0 < NUM_LEDS; ++GPIOR0) {
    i2c_write_byte(64);
  }
  pca9624_stop();
}

ISR(PCINT0_vect)
{
  if (is_pushbutton_pressed()) {
    last_time_button_press = timekeeping_ms_since_reset();
    return;
  }

  if (!timekeeping_ms_passed(last_time_button_press, 750)) {
    // Press was short enough, switch modes.
    if (mode_current == &mode_battery_low) {
      // Do not change out of this mode.
      return;
    }
    if (mode_current == &mode_knight_rider) {
      mode_current = &mode_breathing;
      PORTB |= 1 << PB1;
    }
    else {
      mode_current = &mode_knight_rider;
      PORTB &= ~(1 << PB1);
    }
    mode_current->start();
  }
}

void sleepytime()
{
  // Turn off the LEDs.
  voltreg_enable(false);

  // Keep sleeping when we woke up because the "sleep button" was released.
  bool keep_sleeping;
  do {
    keep_sleeping = is_pushbutton_pressed();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_mode();
  } while (keep_sleeping);

  // Start counting from zero.
  timekeeping_reset();
  last_time_button_press = 0;

  // Reset Timer 0 so it starts counting afresh.
  TIFR = 0;
  TCNT0 = 0;

  boot();
}

// LED order: 7 6 5 4 3 2 1 0
// encoded as values for the PCA9624_reg_ledout type, where 00 = off, 11 = on w/PWM.
static const uint16_t progress_bits[] PROGMEM = {
    0b0000000000000000,  // 0
    0b0000000000000011,  // 1
    0b0000000000001111,  // 2
    0b0000000000111111,  // 3
    0b0000000011111111,  // 4
    0b0000001111111111,  // 5
    0b0000111111111111,  // 6
    0b0011111111111111,  // 7
    0b1111111111111111,  // 8
};
void leds_progress_bar(const uint8_t progress)  // values 0 (off) to 8 (full)
{
  const uint16_t bit_pattern = pgm_read_word(&progress_bits[progress]);
  pca9624_write(PCA9624_REG_LEDOUT0, bit_pattern & 0xFF);
  pca9624_write(PCA9624_REG_LEDOUT1, bit_pattern >> 8);
}

void battery_measure_at_boot()
{
  ADCSRA |= 1 << ADEN;  // enable ADC.

  // Smoothly move towards the battery level. Loading the battery will drop the
  // voltage, which will show less LEDs, which will reduce the load, which will
  // increase the voltage, which will show more LEDs, ....
  float smooth_level = 0.0f;
  for (GPIOR0 = 0; GPIOR0 < 254; ++GPIOR0) {
    const uint8_t level = adc_battery_level();
    smooth_level += (level - smooth_level) * 0.05f;

    leds_progress_bar(smooth_level + 0.5f);
    _delay_ms(4);
  }

  ADCSRA &= ~(1 << ADEN);  // disable the ADC.
}

uint8_t battery_measure()
{
  ADCSRA |= 1 << ADEN;  // enable ADC.

  // Smoothen out any ADC noise from the LED PWMming.
  float smooth_level = 0.0f;
  for (GPIOR0 = 0; GPIOR0 < 10; ++GPIOR0) {
    GPIOR1 = adc_battery_level();
    smooth_level += (GPIOR1 - smooth_level) * 0.1f;
  }

  GPIOR2 = smooth_level;
  ADCSRA &= ~(1 << ADEN);  // disable the ADC.
  return GPIOR2;
}

bool mode_knight_rider_tick()
{
  static uint16_t last_dim_time = 0;
  static uint16_t last_move_time = 0;
  bool brightness_changed = false;

  if (timekeeping_ms_periodic(&last_dim_time, 30)) {
    // Lower the brightness of all LEDs a bit.
    uint8_t *values = pwm_values;
    for (GPIOR0 = 0; GPIOR0 < NUM_LEDS; ++GPIOR0, ++values) {
      *values *= 0.9f;
    }
    brightness_changed = true;
  }

  if (timekeeping_ms_periodic(&last_move_time, 200)) {
    // Make one LED brightest.
    pwm_values[bright_led_index] = 255;
    bright_led_index += bright_led_direction;
    if (bright_led_index >= NUM_LEDS - 1) {
      bright_led_direction *= -1;
    }
    else if (bright_led_index <= 0) {
      bright_led_direction *= -1;
    }
    brightness_changed = true;
  }

  return brightness_changed;
}

void mode_knight_rider_start()
{
}

bool mode_breathing_tick()
{
#define PWM_MIN 24
#define PWM_MAX 64
  static uint8_t breathing_step = 0;
  static uint8_t pwm = PWM_MIN;
  static uint16_t step_start_time = 0;

  switch (breathing_step) {
    case 0:  // Brighten
      if (timekeeping_ms_periodic(&step_start_time, 60)) {
        pwm++;
        if (pwm == PWM_MAX) {
          breathing_step++;
          step_start_time = timekeeping_ms_since_reset();
        }
      }
      break;
    case 1:  // Hold
      if (timekeeping_ms_passed(step_start_time, 3000)) {
        breathing_step++;
      }
      break;
    case 2:  // Darken
      if (timekeeping_ms_periodic(&step_start_time, 30)) {
        pwm--;
        if (pwm == PWM_MIN) {
          breathing_step = 0;
        }
      }
      break;
  }

  pca9624_write(PCA9624_REG_GRPPWM, pwm);
  return false;
}

void mode_breathing_start()
{
  pca9624_start(PCA9624_REG_PWM0 | PCA9624_AI_ALL_REG);
  for (GPIOR0 = 0; GPIOR0 < NUM_LEDS; ++GPIOR0) {
    i2c_write_byte(64);
  }
  i2c_write_byte(16);  // Group brightness.
  pca9624_stop();
}

bool mode_battery_low_tick()
{
  // Don't change anything.
  return false;
}

void mode_battery_low_start()
{
  pca9624_start(PCA9624_REG_PWM0 | PCA9624_AI_ALL_REG);
  for (GPIOR0 = 0; GPIOR0 < NUM_LEDS; ++GPIOR0) {
    i2c_write_byte(GPIOR0 % 2 == 0 ? 0 : 64);
  }
  i2c_write_byte(16);  // Group brightness.
  pca9624_stop();
}
