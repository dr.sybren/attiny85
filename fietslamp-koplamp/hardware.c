/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2022 Sybren A. Stüvel.
 */
#include "hardware.h"

#include <avr/io.h>

void hardware_setup()
{
  // PB0: IN/OUT I2C-SDA
  // PB2: IN/OUT I2C-SCL
  // PB3: OUT    Active-low SHUTDOWN of 3.3v regulator
  // PB4: IN     Pushbutton, needs pull-up
  PORTB = 0              //
          | 1 << PORTB1  // unused PB1, ensure a stable value
          | 1 << PORTB4  // pull-up on PB4 for pushbutton
      ;
  DDRB = 1 << PB3;  // Only OUT on PB3, the rest is handled by the i2c module.
}
