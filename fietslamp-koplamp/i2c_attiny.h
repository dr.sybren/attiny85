#pragma once

#include <stdbool.h>
#include <stdint.h>

void i2c_init();
void i2c_shutdown();

void i2c_start();
void i2c_stop();

typedef enum i2cReadWrite {
  I2C_WRITE = 0,
  I2C_READ = 1,
} i2cReadWrite;

typedef enum i2cAckNack {
  I2C_ACK = 0,
  I2C_NACK = 1,
} i2cAckNack;

/**
 * Start the I2C transaction by writing the address.
 * Keeps looping indefinitely until the address byte is acknowledged.
 */
void i2c_start_address(uint8_t address, i2cReadWrite readWrite);
i2cAckNack i2c_write_address(uint8_t address, i2cReadWrite readWrite);
i2cAckNack i2c_write_byte(uint8_t data);
uint8_t i2c_read_byte(i2cAckNack ackNack);
