/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#include <avr/io.h>
#include <stdbool.h>

#define F_CPU 2000000

// Pinout (the ones that matter at runtime):
// PB0: IN/OUT I2C-SDA
// PB2: OUT    I2C-SCL
// PB3: OUT    Active-low SHUTDOWN of 3.3v regulator
// PB4: IN     Pushbutton, needs pull-up
void hardware_setup();

static inline void voltreg_enable(const bool enable)
{
  if (enable) {
    PORTB |= (1 << PORTB3);
  }
  else {
    PORTB &= ~(1 << PORTB3);
  }
}

static inline bool is_pushbutton_pressed(void)
{
  return (PINB & (1 << PINB4)) == 0;
}
