/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "timekeeping.h"
#include "hardware.h"

#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

static volatile uint16_t msec_count;

void timekeeping_setup()
{
#if F_CPU != 2000000
#  warning Unsupported F_CPU
#endif

  // Timer 0 for keeping track of time at 1000 Hz.
  // Using T0 because it has the least flexibility in clockdiv but it's good
  // enough here.
  TCCR0A = 0b10 << WGM00;  // CTC mode
  TCCR0B = 0               //
           | 0 << WGM02    // CTC mode
           | 2 << CS00     // clkdiv
      ;
  OCR0A = 251;
  TIMSK |= 1 << OCIE0A;

#if 0
  // Debug output on OC0B:
  OCR0B = OCR0A;
  DDRB |= 1 << DDB1;
  TCCR0A |= (0b01 << COM0B0);  // Toggle OC0B on Compare Match.
#endif
}

void timekeeping_reset()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    TCNT0 = 0;
    msec_count = 0;
  }
}

uint16_t timekeeping_ms_since_reset()
{
  int16_t safe_msec_count;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    safe_msec_count = msec_count;
  }
  return safe_msec_count;
}

bool timekeeping_ms_passed(const uint16_t last_time_ms, const uint16_t interval_ms)
{
  const uint16_t now = timekeeping_ms_since_reset();
  const uint16_t expired = now - last_time_ms;
  const bool has_passed = expired > interval_ms;
  return has_passed;
}

bool timekeeping_ms_periodic(uint16_t *last_time_ms, const uint16_t interval_ms)
{
  const uint16_t now = timekeeping_ms_since_reset();
  const uint16_t expired = now - *last_time_ms;
  const bool has_passed = expired >= interval_ms;
  if (has_passed) {
    *last_time_ms = now;
  }
  return has_passed;
}

ISR(TIMER0_COMPA_vect)
{
  const uint8_t sreg = SREG;

  TCNT0 = 0;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    msec_count++;
  }

  SREG = sreg;
}
