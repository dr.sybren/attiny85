/* Copyright (C) 2021 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "adc.h"
#include "hardware.h"

#include <avr/interrupt.h>
#include <avr/sleep.h>

void adc_setup(void)
{
  ADMUX = 0                 //
          | 0b00 << REFS0   // VCC as voltage ref
          | 0 << REFS2      //
          | 0 << ADLAR      // Left-adjust
          | 0b1100 << MUX0  // Read 1.1v internal bandgap voltage
      ;

  ADCSRA = 0                 //
           | 0 << ADEN       // Don't enable ADC at boot, enable it before use.
           | 1 << ADIE       // ADC interrupt enable
           | 0b100 << ADPS0  // scale ADC clock so it's 50-200 KHz
      ;
  ADCSRB = 0;
}

ISR(ADC_vect)
{
}

static uint16_t adc_read()
{
  ADCSRA |= (1 << ADSC);
  while ((ADCSRA & (1 << ADSC)) == 0) {
    set_sleep_mode(SLEEP_MODE_ADC);
    sleep_mode();
  }
  return ADC;
}

uint8_t adc_battery_level()
{
  const uint16_t adc = adc_read();

  // ADC measures 1.1v with Vcc as reference,
  // so lower ADC means Vcc is higher.
  const uint16_t ADC_LOW = 0x0158;   // 3.3v level
  const uint16_t ADC_HIGH = 0x0116;  // 4.2v level

  if (adc >= ADC_LOW)
    return 1;
  if (adc <= ADC_HIGH)
    return 8;

  const uint8_t input_range = ADC_LOW - ADC_HIGH;
  const uint8_t level = (float)(ADC_LOW - adc) * 8.0f / input_range + 1.0f;
  return level;
}
