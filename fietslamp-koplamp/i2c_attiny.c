#include "i2c_attiny.h"
#include "hardware.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>
#include <util/delay.h>

#define PIN_SDA PB0
#define PIN_SCL PB2

#define WAIT_LONG 5   // 4,7us
#define WAIT_SHORT 4  // 4,0us

static inline bool read_sda();
static inline bool read_scl();
static inline void release_sda();  // Let the pull-up resistors pull up.
static inline void release_scl();  // Let the pull-up resistors pull up.
static inline void pull_low_sda();  // Set to low output.
static inline void pull_low_scl();  // Set to low output.
static inline void ddr_sda_out();
static inline void ddr_scl_out();
static inline void ddr_sda_in();
static inline void ddr_scl_in();

void i2c_init()
{
  release_scl();
  release_sda();
}

void i2c_shutdown()
{
  ddr_sda_in();
  ddr_scl_in();
}

void i2c_start_address(const uint8_t address, const i2cReadWrite readWrite)
{
  i2c_start();
  while (i2c_write_address(address, readWrite)) {
    i2c_stop();
    i2c_start();
  }
}

void i2c_start()
{
  // Release both lines, and wait for any other device to do the same.
  release_sda(true);
  release_scl(true);
  while (!read_scl() && !read_sda())  // Wait until released.
    ;

  // START means lowering SDA with SCL high.
  pull_low_sda();  // start condition
  _delay_us(WAIT_LONG);
  pull_low_scl();
}

void i2c_stop()
{
  pull_low_sda();
  release_scl();
  while (!read_scl())  // Wait until released.
    ;
  _delay_us(WAIT_LONG);

  release_sda();
  while (!read_sda())  // Wait until released.
    ;
  _delay_us(WAIT_SHORT);
}

i2cAckNack i2c_write_address(const uint8_t address, const i2cReadWrite readWrite)
{
  return i2c_write_byte((address << 1) | (readWrite == I2C_READ));
}

i2cAckNack i2c_write_byte(uint8_t data)
{
  for (uint8_t n = 0; n < 8; ++n) {
    pull_low_scl();
    if (data & 0x80) {
      release_sda();
    }
    else {
      pull_low_sda();
    }
    _delay_us(WAIT_LONG);

    release_scl();
    while (!read_scl())  // Wait for SCL to go high
      ;
    _delay_us(WAIT_SHORT);

    data <<= 1;
  }

  // Release SDA to read the ACK bit and do one final clock pulse.
  pull_low_scl();
  release_sda();
  _delay_us(WAIT_LONG);
  release_scl();
  while (!read_scl())  // Wait for SCL to go high
    ;

  const bool ack_bit = read_sda();
  _delay_us(WAIT_SHORT);

  pull_low_scl();
  pull_low_sda();

  // Pulled low means ACK.
  return ack_bit ? I2C_NACK : I2C_ACK;
}

uint8_t i2c_read_byte(const i2cAckNack ackNack)
{
  uint8_t data = 0;

  for (uint8_t n = 0; n < 8; ++n) {
    pull_low_scl();
    release_sda();  // so other device can control its value
    _delay_us(WAIT_LONG);

    release_scl();
    while (!read_scl())  // Wait for SCL to go high
      ;
    data = (data << 1) | read_sda();
    _delay_us(WAIT_SHORT);
  }

  // Send the ACK bit and do one final clock pulse.
  pull_low_scl();
  if (ackNack == I2C_ACK) {
    pull_low_sda();
  }
  else {
    release_sda();
  }
  _delay_us(WAIT_LONG);
  release_scl();
  while (!read_scl())  // Wait for SCL to go high
    ;
  _delay_us(WAIT_SHORT);

  pull_low_scl();
  pull_low_sda();

  return data;
}

static inline void ddr_sda_out()
{
  DDRB |= (1 << PIN_SDA);
}
static inline void ddr_scl_out()
{
  DDRB |= (1 << PIN_SCL);
}
static inline void ddr_sda_in()
{
  DDRB &= ~(1 << PIN_SDA);
}
static inline void ddr_scl_in()
{
  DDRB &= ~(1 << PIN_SCL);
}
static inline bool read_sda()
{
  return (PINB & (1 << PIN_SDA)) != 0;
}
static inline bool read_scl()
{
  return (PINB & (1 << PIN_SCL)) != 0;
}
static inline void release_sda()
{
  ddr_sda_in();
  PORTB &= ~(1 << PIN_SDA);
}
static inline void release_scl()
{
  ddr_scl_in();
  PORTB &= ~(1 << PIN_SCL);
}
static inline void pull_low_sda()
{
  PORTB &= ~(1 << PIN_SDA);
  ddr_sda_out();
}
static inline void pull_low_scl()
{
  PORTB &= ~(1 << PIN_SCL);
  ddr_scl_out();
}
