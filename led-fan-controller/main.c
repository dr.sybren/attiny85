/* LED & Fan Controller
 * Copyright (C) 2020 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <inttypes.h>
#include <stdbool.h>
#include <util/atomic.h>

/** Pin-out:
 * PB0: MOSI & LED PWM
 * PB1: MISO & Fan PWM
 * PB2: SCK
 * PB4: Analogue value Fan
 * PB5: Analogue value LED
 */

inline static void fan_pwm_enable()
{
  PORTB &= ~(1 << PORTB1);
  TCCR1 = 0                 // start with nothing
          | 1 << PWM1A      // Enable PWM A
          | 0b10 << COM1B0  // normal PWM on OC1A (PB1)
          | 0b0001 << CS10  // clkdiv
      ;
}
inline static void fan_full_on()
{
  TCCR1 = 0;
  PORTB |= 1 << PORTB1;
}
inline static void fan_full_off()
{
  TCCR1 = 0;
  PORTB &= ~(1 << PORTB1);
}

void setup_pwm()
{
  // Pins PB0 (OC0A) and PB1 (OC1A) get PWM.

  // LED PWM:
  TCCR0A = 0                 // start with nothing
           | 0b10 << COM0A0  // normal PWM mode on OC0A (PB0)
           | 0b11 << WGM00   // fast PWM mode
      ;
  TCCR0B = 0                // start with nothing
           | 0 << WGM02     // fast PWM mode
           | 0b010 << CS00  // clkdiv
      ;

  // FAN PWM:
  fan_pwm_enable();
  GTCCR = 0;
  PLLCSR = 0;

  OCR0A = 0;
  OCR1A = 20;
}

void setup_adc()
{
  ADCSRA = 0                 // start with nothing
           | 1 << ADEN       // Enable ADC
           | 0b111 << ADPS0  // scale ADC clock
      ;
}

void setup_watchdog()
{
  MCUSR &= ~(1 << WDRF);
  WDTCR = 1 << WDCE | 1 << WDE;
  WDTCR = 1 << WDIE | 0b0001 << WDP0;
}

void setup()
{
  // Disable clock divider:
  CLKPR = 1 << CLKPCE;
  CLKPR = 0b0000;

  // PB0 and PB1 are output
  // PB4 and PB5 are input
  DDRB = 0 | 1 << DDB0 | 1 << DDB1;
  PORTB = 0;

  setup_pwm();
  setup_adc();
  setup_watchdog();
}

ISR(ADC_vect)
{
}

void readADC()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  ADCSRA |= 1 << ADSC | 1 << ADIE;
  sleep_mode();
}

volatile bool fan_revving_up = true;

ISR(WDT_vect)
{
  if (OCR1A < 100) {
    // Still revving up.
    OCR1A += 3;
    return;
  }

  // Done revving up, disable watchdog.
  fan_revving_up = false;
  WDTCR = 1 << WDCE | 0 << WDE;
}

void control_fan()
{
  if (fan_revving_up) {
    return;
  }

  // Read from ADC2 == PB4
  ADMUX = 0b0010 << MUX0 | 1 << ADLAR;
  readADC();

  if (ADCH > 240) {
    fan_full_on();
    return;
  }
  fan_pwm_enable();
  if (ADCH == 0) {
    OCR1A = 2;
    return;
  }

  OCR1A = ADCH * (255.0f / 240.0f);
}

void control_led()
{
  // Read from ADC3 == PB3
  ADMUX = 0b0011 << MUX0 | 1 << ADLAR;
  readADC();
  OCR0A = ADCH;
}

int main()
{
  setup();

  sei();
  for (;;) {
    control_fan();
    control_led();
  }
  return 0;
}
