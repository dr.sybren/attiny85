# Keukenlicht ATtiny85

This is a controller for an RGB LED strip. I use it in my kitchen ('keuken' in Nederlands), hence the name. It provides a single rotary knob to control both the intensity and the warmth of the light. When the light intensity is low, it has a warm colour, and when it becomes brighter it also becomes whiter/cooler.

### [Demo video on YouTube](https://www.youtube.com/watch?v=1JVtGqv783g)

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0; float: right" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

The code in this project is licensed under the GPL v3 license.

The non-code parts of this work are licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
