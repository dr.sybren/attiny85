/* Keukenlicht-ATtiny85
 * Copyright (C) 2019 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "hsv.h"

#include <string.h>
#include <math.h>


const float hsv_lut_keys[] = {0.0, 0.128623, 0.25, 0.75, 0.85, 1.0};
const HSV hsv_lut[] = {
  {0.0497, 1.0, 0.0000},
  {0.0229, 1.0, 0.1536},
  {0.0385, 1.0, 0.8},
  {0.0715, 0.7, 1.0},
  {0.0943, 0.5, 1.0},
  {0.0943, 0.0, 1.0},
};
const int hsv_lut_size = sizeof(hsv_lut_keys) / sizeof(hsv_lut_keys[0]);

void interpolate_lut(float value, int index_high, HSV r_hsv) {
  float lut_key_low = hsv_lut_keys[index_high-1];
  float lut_key_high = hsv_lut_keys[index_high];

  float alpha = (lut_key_high - value) / (lut_key_high - lut_key_low);
  float beta = 1.0f - alpha;

  const HSV *hsv_low = &hsv_lut[index_high-1];
  const HSV *hsv_high = &hsv_lut[index_high];

  r_hsv[0] = alpha * (*hsv_low)[0] + beta * (*hsv_high)[0];
  r_hsv[1] = alpha * (*hsv_low)[1] + beta * (*hsv_high)[1];
  r_hsv[2] = alpha * (*hsv_low)[2] + beta * (*hsv_high)[2];
}

void find_hsv(float input, HSV r_hsv)
{
  int lut_idx;
  float lut_key_value;

  for (lut_idx=0; lut_idx < hsv_lut_size; ++lut_idx) {
    lut_key_value = hsv_lut_keys[lut_idx];

    if (input == lut_key_value) {
      // Found an exact match.
      memcpy(r_hsv, hsv_lut[lut_idx], sizeof(HSV));
      return;
    }
    if (lut_key_value > input) {
      // Found the first LUT key that is bigger than the input.
      interpolate_lut(input, lut_idx, r_hsv);
      return;
    }
  }

  // We didn't find anything, go for the max.
  memcpy(r_hsv, hsv_lut[hsv_lut_size-1], sizeof(HSV));
}

void hsv_to_rgb(HSV hsv, RGB r_rgb) {
  // Source: https://www.rapidtables.com/convert/color/hsv-to-rgb.html
  float c = hsv[1] * hsv[2];
  float h = hsv[0];
  int h360 = h * 360;
  float h_mod = fabs(fmod(h * 360.0f / 60.0f, 2) - 1);
  float x = c * (1.0f - h_mod);
  float m = hsv[2] - c;

  float r_, g_, b_;
  if (h360 < 60) {
    r_ = c; g_ = x; b_ = 0;
  }
  else if (h360 < 120) {
    r_ = x; g_ = c; b_ = 0;
  }
  else if (h360 < 180) {
    r_ = 0; g_ = c; b_ = x;
  }
  else if (h360 < 240) {
    r_ = 0; g_ = x; b_ = c;
  }
  else if (h360 < 300) {
    r_ = x; g_ = 0; b_ = c;
  }
  else {
    r_ = c; g_ = 0; b_ = x;
  }

  r_rgb[0] = 255 * (r_ + m);
  r_rgb[1] = 255 * (g_ + m);
  r_rgb[2] = 255 * (b_ + m);
}
