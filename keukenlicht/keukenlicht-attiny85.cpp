/* Keukenlicht-ATtiny85
 * Copyright (C) 2019 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>

#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <avr/io.h>

#include "hsv.h"

volatile bool adc_value_changed = false;
volatile uint8_t last_adc = 0;


void setup_pwm()
{
  // Pins PB0 (OCR0A), PB1 (OCR1A), and PB4 (OCR1B) get PWM.
  TCCR0A = 3<<WGM00 | 0b11<<COM0A0 | 0<<COM0B0;
  TCCR0B = 0<<WGM02 | 0b10<<CS00;
  TCCR1 = 1<<CTC1 | 1<<PWM1A | 0b0100<<CS10 | 0b11<<COM1A0;
  GTCCR = 1<<PWM1B | 0b11<<COM1B0;
}

void pwm_off()
{
  TCCR0A = 0;
  TCCR0B = 0;
  TCCR1  = 0;
  GTCCR  = 0;
}

void setup_adc()
{
  // The voltage reference for the ADC may be selected by writing to the REFS[2:0] bits in ADMUX.
  // VCC as voltage ref = 000, so REFS[0:2] doesn't appear here.

  // The analog input channel and differential gain are selected by writing to the MUX[3:0] bits in ADMUX.
  // Any of the four ADC input pins ADC[3:0] can be selected as single ended inputs to the ADC
  // Using pin PB5 = 0000, so MUX[3:0] doesn't appear here.

  // The ADC is enabled by setting the ADC Enable bit, ADEN in ADCSRA.
  // ADPS0 controls the ADC clock prescaler.
  ADCSRA = 1 << ADEN | 3 << ADPS0;

  // The ADC generates a 10-bit result which is presented in the ADC Data Registers, ADCH and ADCL.
  // By default, the result is presented right adjusted, but can optionally be presented left adjusted
  // by setting the ADLAR bit in ADMUX. If the result is left adjusted and no more than 8-bit precision
  // is required, it is sufficient to read ADCH.

  // Read from ADC3 == PB3
  ADMUX = 3 << MUX0 | 1 << ADLAR;
}

void setup_watchdog()
{
  MCUSR &= ~(1<<WDRF);
  WDTCR = 1<<WDCE | 1<<WDE;
  WDTCR = 1<<WDIE | 5<<WDP0;

  // Disable unused interrupts so we can sleep.
  GIMSK = 0;
  GIFR = 0;
  PCMSK = 0;
  TIMSK = 0;
  TIFR = 0;
}

void setup()
{
  // Set LED pins as output
  DDRB = 1 << DDB0 | 1 << DDB1 | 1 << DDB4;

  setup_pwm();
  setup_adc();
  setup_watchdog();

  OCR0A = 0;
  OCR1A = 0;
  OCR1B = 0;
}

ISR(ADC_vect)
{
  uint8_t adch = ADCH;
  adc_value_changed = adch != last_adc;
  last_adc = adch;
}

// ISR just needs to exist for the interrupt to wake us up.
ISR(WDT_vect) {}

void readADC()
{
  set_sleep_mode(SLEEP_MODE_IDLE);

  // A single conversion is started by writing a logical one to the ADC Start Conversion bit, ADSC.
  ADCSRA |= 1 << ADSC | 1 << ADIE;

  sleep_mode();
}

#define MIN_ADCH 20
int unchanged_iterations = 0;

void loop()
{
  readADC();

  float input = ((float)(ADCH - MIN_ADCH)) / (255.f - MIN_ADCH);
  static float smoothed_input = 0.f;

  float step_size = (input - smoothed_input) / 20.f;
  smoothed_input += step_size;
  smoothed_input = fmax(smoothed_input, 0.0f);

  if (fabs(step_size) < 0.0001f) {
    if (unchanged_iterations < 2000) {
      unchanged_iterations++;
    }
    else {
      // Sleep, wake up by watchdog timer.
      if (smoothed_input < 0.001f && TCCR0A) pwm_off();
      set_sleep_mode(SLEEP_MODE_IDLE);
      sleep_mode();
    }
    return;
  }
  unchanged_iterations = 0;

  HSV target_hsv;
  find_hsv(smoothed_input, target_hsv);

  RGB target_rgb;
  hsv_to_rgb(target_hsv, target_rgb);
  OCR0A = 255 - target_rgb[0];
  OCR1A = 255 - target_rgb[1];
  OCR1B = 255 - target_rgb[2];
  // OCR0A = 255 * input;
  // OCR1A = 255 * (unchanged_iterations / 2000.f);
  // OCR1B = 255 * smoothed_input;
  if (TCCR0A == 0) setup_pwm();
}

int main()
{
  sei();
  setup();
  for(;;) loop();
  return 0;
}
