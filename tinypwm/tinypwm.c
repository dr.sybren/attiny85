/* Keukenlicht-ATtiny85
 * Copyright (C) 2019 dr. Sybren A. Stüvel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>

#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <avr/io.h>

void setup_pwm()
{
  // Pins PB0 (OCR0A) get PWM.
  TCCR0A = 0
    | 0b11 << COM0A0  // inverting PWM mode on OCR0A
    | 0b00 << COM0B0  // OCR0B disabled
    | 0b11 << WGM00   // fast PWM mode
  ;
  TCCR0B = 0
    | 0 << WGM02    // fast PWM mode
    | 0b010 << CS00 // clk/8
  ;
  TCCR1 = 0;
  GTCCR = 0;
}

void setup_adc()
{
  ADCSRA = 0
    | 1 << ADEN  // Enable ADC
    | 0b011 << ADPS0 // scale ADC clock
    | 0b000 << REFS0;  // VCC as voltage ref

  // Read from ADC3 == PB3
  ADMUX = 3 << MUX0 | 1 << ADLAR;
}

void setup()
{
  DDRB = 0
    | 1 << DDB0
    | 1 << DDB1
    | 1 << DDB4
  ;
  PORTB = 0;

  setup_pwm();
  setup_adc();

  OCR0A = 0;
}

ISR(ADC_vect) {}

void readADC()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  ADCSRA |= 1 << ADSC | 1 << ADIE;
  sleep_mode();
}

void loop()
{
  readADC();

  // float input = ((float)ADCH) / 255.f;
  // float smoothed_input = input;

  uint8_t pwm_value = ADCH;
  OCR0A = 255 - pwm_value;
}

int main()
{
  sei();
  setup();
  for(;;) loop();
  return 0;
}
